import React from "react";
import Actions from "../components/Actions";
import Body from "../components/Body";
import Footer from "../components/Footer";
import Jumbotron from "../components/Jumbotron";

const Home = () => {
  return (
    <>
      <Jumbotron />
      <Actions />
      <Body />
      <Footer />
    </>
  );
};

export default Home;
