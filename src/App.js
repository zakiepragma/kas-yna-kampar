import React from "react";
import { YnaProvider } from "./context/YnaContext";
import Home from "./pages/Home";

const App = () => {
  return (
    <YnaProvider>
      <Home />
    </YnaProvider>
  );
};

export default App;
