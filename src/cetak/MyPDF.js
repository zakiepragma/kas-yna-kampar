import React from "react";
import { Document, Page, Text, View, StyleSheet } from "@react-pdf/renderer";

const styles = StyleSheet.create({
  view: {
    padding: 5,
  },
  table: {
    display: "table",
    width: "100%",
    borderStyle: "solid",
    borderWidth: 1,
    borderRightWidth: 0,
    borderBottomWidth: 0,
    textAlign: "center",
    fontSize: 10,
  },
  tableRow: {
    flexDirection: "row",
  },
  tableColHeader: {
    width: "30%",
    borderStyle: "solid",
    borderBottomWidth: 1,
    borderRightWidth: 1,
    backgroundColor: "#e4e4e4",
    padding: 5,
  },
  tableColHeaderUraian: {
    width: "47.77%",
    borderStyle: "solid",
    borderBottomWidth: 1,
    borderRightWidth: 1,
    backgroundColor: "#e4e4e4",
    padding: 5,
  },
  tableColHeaderNumber: {
    width: "11.11%",
    borderStyle: "solid",
    borderBottomWidth: 1,
    borderRightWidth: 1,
    backgroundColor: "#e4e4e4",
    padding: 5,
  },
  tableColData: {
    width: "30%",
    borderStyle: "solid",
    borderBottomWidth: 1,
    borderRightWidth: 1,
    alignItems: "center",
    justifyContent: "center",
    padding: 5,
  },
  tableColDataNumber: {
    width: "11.11%",
    borderStyle: "solid",
    borderBottomWidth: 1,
    borderRightWidth: 1,
    alignItems: "center",
    justifyContent: "center",
    padding: 5,
  },
  tableColDataUraian: {
    width: "47.77%",
    borderStyle: "solid",
    borderBottomWidth: 1,
    borderRightWidth: 1,
    alignItems: "center",
    justifyContent: "center",
    padding: 5,
  },
  tableColDataSpan: {
    width: "50%",
    borderStyle: "solid",
    borderBottomWidth: 1,
    borderRightWidth: 1,
    alignItems: "center",
    justifyContent: "center",
    padding: 5,
  },
  tableColDataSpanTotal: {
    width: "50%",
    borderStyle: "solid",
    borderBottomWidth: 1,
    borderRightWidth: 1,
    alignItems: "center",
    justifyContent: "center",
    padding: 5,
  },
});

function getIndonesianDay(dateString) {
  const days = ["Ahad", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"];

  const [day, month, year] = dateString.split("-");
  const isoDateString = [year, month, day].join("-");

  const date = new Date(isoDateString);
  const dayIndex = date.getDay();

  if (isNaN(dayIndex)) {
    console.log(`Invalid date string: ${dateString}`);
    return "";
  }

  return days[dayIndex];
}

function formatRupiah(angka) {
  let rupiah = "";
  const angkarev = angka.toString().split("").reverse().join("");
  for (let i = 0; i < angkarev.length; i++)
    if (i % 3 == 0) rupiah += angkarev.substr(i, 3) + ".";
  return (
    "Rp " +
    rupiah
      .split("", rupiah.length - 1)
      .reverse()
      .join("")
  );
}

const MyPDF = ({ dataFilter, totalSaldo, totalSaldoSebelum2023 }) => (
  <Document>
    <Page>
      <View style={styles.view}>
        <Text style={{ textAlign: "center", fontSize: 18, marginBottom: 5 }}>
          Kas Yayasan Nida As-Sunnah
        </Text>
        <Text style={{ textAlign: "center", fontSize: 15, marginBottom: 10 }}>
          2023 - Sekarang
        </Text>
        <View style={styles.table}>
          <View style={styles.tableRow}>
            <View style={styles.tableColHeaderNumber}>
              <Text style={{ fontWeight: "bold" }}>#</Text>
            </View>
            <View style={styles.tableColHeaderUraian}>
              <Text style={{ fontWeight: "bold" }}>Uraian</Text>
            </View>
            <View style={styles.tableColHeader}>
              <Text style={{ fontWeight: "bold" }}>Tanggal</Text>
            </View>
            <View style={styles.tableColHeader}>
              <Text style={{ fontWeight: "bold" }}>Pemasukan</Text>
            </View>
            <View style={styles.tableColHeader}>
              <Text style={{ fontWeight: "bold" }}>Pengeluaran</Text>
            </View>
          </View>
          {dataFilter.map((data, index) => (
            <View style={styles.tableRow} key={index}>
              <View style={styles.tableColDataNumber}>
                <Text>{index + 1}</Text>
              </View>
              <View style={styles.tableColDataUraian}>
                <Text>{data.uraian}</Text>
              </View>
              <View style={styles.tableColData}>
                <Text>
                  {getIndonesianDay(data.tanggal) + ", " + data.tanggal}
                </Text>
              </View>
              <View style={styles.tableColData}>
                <Text>{formatRupiah(data.pemasukan)}</Text>
              </View>
              <View style={styles.tableColData}>
                <Text>{formatRupiah(data.pengeluaran)}</Text>
              </View>
            </View>
          ))}
          <View style={styles.tableRow}>
            <View style={styles.tableColDataSpanTotal}>
              <Text>Total Pemasukan</Text>
            </View>
            <View style={styles.tableColDataSpan}>
              <Text>
                {formatRupiah(
                  dataFilter.reduce((total, data) => total + data.pemasukan, 0)
                )}
              </Text>
            </View>
          </View>
          <View style={styles.tableRow}>
            <View style={styles.tableColDataSpanTotal}>
              <Text>Total Pengeluaran</Text>
            </View>
            <View style={styles.tableColDataSpan}>
              <Text>
                {formatRupiah(
                  dataFilter.reduce(
                    (total, data) => total + data.pengeluaran,
                    0
                  )
                )}
              </Text>
            </View>
          </View>
          <View style={styles.tableRow}>
            <View style={styles.tableColDataSpanTotal}>
              <Text>Total Saldo 2023</Text>
            </View>
            <View style={styles.tableColDataSpan}>
              <Text>
                {formatRupiah(
                  dataFilter.reduce(
                    (total, data) => total + data.pemasukan - data.pengeluaran,
                    0
                  )
                )}
              </Text>
            </View>
          </View>
          <View style={styles.tableRow}>
            <View style={styles.tableColDataSpanTotal}>
              <Text>Total Saldo Sebelum 2023</Text>
            </View>
            <View style={styles.tableColDataSpan}>
              <Text>{formatRupiah(totalSaldoSebelum2023)}</Text>
            </View>
          </View>
          <View style={styles.tableRow}>
            <View style={styles.tableColDataSpanTotal}>
              <Text>Total Saldo Saat Ini</Text>
            </View>
            <View style={styles.tableColDataSpan}>
              <Text>
                {formatRupiah(
                  totalSaldoSebelum2023 +
                    dataFilter.reduce(
                      (total, data) =>
                        total + data.pemasukan - data.pengeluaran,
                      0
                    )
                )}
              </Text>
            </View>
          </View>
        </View>
      </View>
    </Page>
  </Document>
);

export default MyPDF;
