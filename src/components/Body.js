import React, { useState } from "react";
import { useYna } from "../context/YnaContext";
import CurrencyFormat from "react-currency-input-field";
import Pagination from "react-js-pagination";

const Body = () => {
  const { dataFilter } = useYna();

  const totalIncome = dataFilter.reduce(
    (total, item) => total + item.pemasukan,
    0
  );
  const totalExpense = dataFilter.reduce(
    (total, item) => total + item.pengeluaran,
    0
  );
  const totalBalance = totalIncome - totalExpense;

  function getIndonesianDay(dateString) {
    const days = ["Ahad", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"];

    // Reverse the date string to YYYY-MM-DD format
    const [day, month, year] = dateString.split("-");
    const isoDateString = [year, month, day].join("-");

    const date = new Date(isoDateString);
    const dayIndex = date.getDay();

    if (isNaN(dayIndex)) {
      console.log(`Invalid date string: ${dateString}`);
      return "";
    }

    return days[dayIndex];
  }

  const [currentPage, setCurrentPage] = useState(1);
  const itemsPerPage = 10;

  const handlePageChange = (pageNumber) => {
    setCurrentPage(pageNumber);
  };

  const indexOfLastItem = currentPage * itemsPerPage;
  const indexOfFirstItem = indexOfLastItem - itemsPerPage;
  const currentData = dataFilter.slice(indexOfFirstItem, indexOfLastItem);

  return (
    <div className="flex flex-col px-5 mb-12 pb-12">
      <div className="overflow-x-auto">
        <table className="mx-auto w-full rounded-lg bg-white divide-y divide-gray-300 overflow-hidden">
          <thead className="bg-gray-50">
            <tr className="text-gray-600 text-left">
              <th className="font-semibold text-sm uppercase px-4 py-4">#</th>
              <th className="font-semibold text-sm uppercase px-4 py-4">
                Uraian
              </th>
              <th className="font-semibold text-sm uppercase px-4 py-4">
                Tanggal
              </th>
              <th className="font-semibold text-sm uppercase px-4 py-4">
                Pemasukan
              </th>
              <th className="font-semibold text-sm uppercase px-4 py-4">
                Pengeluaran
              </th>
              <th className="font-semibold text-sm uppercase px-4 py-4">
                Saldo
              </th>
            </tr>
          </thead>
          <tbody className="divide-y divide-gray-200">
            {currentData.map((item, index) => {
              const saldo =
                index === 0
                  ? item.pemasukan - item.pengeluaran
                  : dataFilter
                      .slice(0, index)
                      .reduce(
                        (acc, cur) => acc + cur.pemasukan - cur.pengeluaran,
                        0
                      ) +
                    item.pemasukan -
                    item.pengeluaran;
              return (
                <tr key={index} className="text-gray-700">
                  <td className="px-4 py-4">{index + indexOfFirstItem + 1}</td>
                  <td className="px-4 py-4">{item.uraian}</td>
                  <td className="px-4 py-4">
                    {getIndonesianDay(item.tanggal) + ", " + item.tanggal}
                  </td>
                  <td className="px-4 py-4">
                    <CurrencyFormat
                      value={item.pemasukan}
                      prefix={"Rp "}
                      className="p-2"
                      style={{
                        width: `${item.pemasukan.toString().length + 7}ch`,
                      }}
                    />
                  </td>
                  <td className="px-4 py-4">
                    <CurrencyFormat
                      value={item.pengeluaran}
                      prefix={"Rp "}
                      className="p-2"
                      style={{
                        width: `${item.pengeluaran.toString().length + 7}ch`,
                      }}
                    />
                  </td>
                  <td className="px-4 py-4">
                    <CurrencyFormat
                      value={saldo}
                      prefix={"Rp "}
                      className="p-2"
                      style={{ width: `${saldo.toString().length + 7}ch` }}
                    />
                  </td>
                </tr>
              );
            })}
            <tr className="font-bold">
              <td colSpan="3" className="px-4 py-4">
                Total
              </td>
              <td className="px-4 py-4">
                <CurrencyFormat
                  value={totalIncome}
                  prefix={"Rp "}
                  className="p-2"
                  style={{ width: `${totalIncome.toString().length + 7}ch` }}
                />
              </td>
              <td className="px-4 py-4">
                <CurrencyFormat
                  value={totalExpense}
                  prefix={"Rp "}
                  className="p-2"
                  style={{ width: `${totalExpense.toString().length + 7}ch` }}
                />
              </td>
              <td className="px-4 py-4">
                <CurrencyFormat
                  value={totalBalance}
                  prefix={"Rp "}
                  className="p-2"
                  style={{ width: `${totalBalance.toString().length + 7}ch` }}
                />
              </td>
            </tr>
          </tbody>
        </table>
        <Pagination
          activePage={currentPage}
          itemsCountPerPage={itemsPerPage}
          totalItemsCount={dataFilter.length}
          pageRangeDisplayed={10}
          onChange={handlePageChange}
          itemClass="pagination"
          linkClass="page-link"
          activeClass="active"
        />
      </div>
    </div>
  );
};

export default Body;
