import React from "react";
import { FaYoutube, FaGithub, FaGitlab } from "react-icons/fa";

const Jumbotron = () => {
  return (
    <div className="py-10 bg-gradient-to-l from-green-800 to-green-400">
      <div className="container mx-auto flex flex-col md:flex-row items-center justify-between px-10">
        <div className="md:w-1/2 mb-10 md:mb-0">
          <h1 className="text-5xl font-bold text-gray-800">
            Yayasan Nida' As-Sunnah Kampar
          </h1>
          <p className="text-2xl text-gray-600 mt-5">2023 - Sekarang</p>
        </div>
        <div className="md:w-1/2 flex justify-center">
          <div className="flex space-x-6">
            <a
              href="https://www.youtube.com/watch?v=NKUSBy7MvXQ&list=PLGwA21JLpwoMtXpnQHnhv0kasbL2tXWlt&index=41"
              target="_blank"
              rel="noopener noreferrer"
            >
              <FaYoutube className="text-red-500 h-8 w-8 hover:text-red-600 transition duration-300" />
            </a>
            <a
              href="https://github.com/programmercintasunnah"
              target="_blank"
              rel="noopener noreferrer"
            >
              <FaGithub className="text-gray-800 h-8 w-8 hover:text-gray-900 transition duration-300" />
            </a>
            <a
              href="https://gitlab.com/zakiepragma"
              target="_blank"
              rel="noopener noreferrer"
            >
              <FaGitlab className="text-orange-500 h-8 w-8 hover:text-orange-600 transition duration-300" />
            </a>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Jumbotron;
