import React, { useState } from "react";
import { FaFilePdf } from "react-icons/fa";
import CurrencyFormat from "react-currency-input-field";
import { useYna } from "../context/YnaContext";
import { PDFDownloadLink } from "@react-pdf/renderer";
import MyPDF from "../cetak/MyPDF";

const Footer = () => {
  const { dataFilter, totalSaldoSebelum2023, totalSaldo } = useYna();

  const fileName = `Kas Yayasan - ${new Date().toLocaleDateString("id-ID")}`;

  return (
    <footer className="bg-gray-200 py-4 px-4 fixed bottom-0 w-full">
      <div className="container mx-auto flex justify-between items-center">
        <h2 className="text-gray-600 text-lg font-semibold">
          Total saldo :{" "}
          <CurrencyFormat
            value={totalSaldoSebelum2023 + totalSaldo}
            prefix={"Rp "}
            className="p-2"
          />
        </h2>
        <PDFDownloadLink
          document={
            <MyPDF
              dataFilter={dataFilter}
              totalSaldoSebelum2023={totalSaldoSebelum2023}
              totalSaldo={totalSaldo}
            />
          }
          className="bg-red-500 hover:bg-red-600 text-black rounded-md p-2 md:ml-auto"
          fileName={fileName}
        >
          {({ blob, url, loading, error }) =>
            loading ? (
              "Loading document..."
            ) : (
              <FaFilePdf className="text-red-100 h-4 w-4 hover:text-red-100 transition duration-300" />
            )
          }
        </PDFDownloadLink>
      </div>
    </footer>
  );
};

export default Footer;
