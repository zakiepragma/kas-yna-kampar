import React from "react";
import { useYna } from "../context/YnaContext";

const Actions = () => {
  const {
    selectKhusus,
    selectType,
    selectBulan,
    filterKhusus,
    filterType,
    filterBulan,
    setSortType,
  } = useYna();

  const handleChangeKhusus = (event) => {
    filterKhusus(event.target.value);
  };

  const handleChangeType = (event) => {
    filterType(event.target.value);
  };

  const handleChangeBulan = (event) => {
    const bulanTahun = event.target.value;
    filterBulan(bulanTahun);
  };

  return (
    <div className="p-5 flex flex-wrap justify-between items-center">
      <div className="mb-3">
        <p className="text-xl font-bold">Kas Yayasan</p>
      </div>
      <div className="flex flex-wrap items-center gap-3">
        <select
          onChange={(e) => setSortType(e.target.value)}
          className="rounded-md border border-gray-300 py-2 px-4"
        >
          <option value="palingLama">Paling Lama</option>
          <option value="palingBaru">Paling Baru</option>
        </select>
        <select
          onChange={handleChangeKhusus}
          className="rounded-md border border-gray-300 py-2 px-4"
        >
          <option value="">Select Khusus</option>
          {selectKhusus.map((s) => (
            <option key={s.id} value={s.id}>
              {s.nama}
            </option>
          ))}
        </select>
        <select
          onChange={handleChangeType}
          className="rounded-md border border-gray-300 py-2 px-4"
        >
          <option value="">Select Type</option>
          {selectType.map((s) => (
            <option key={s.id} value={s.id}>
              {s.nama}
            </option>
          ))}
        </select>
        <select
          onChange={handleChangeBulan}
          className="rounded-md border border-gray-300 py-2 px-4"
        >
          <option value="">Select Bulan</option>
          {selectBulan.map((s) => (
            <option key={s.id} value={s.id}>
              {s.nama}
            </option>
          ))}
        </select>
      </div>
    </div>
  );
};

export default Actions;
