import { createContext, useContext, useEffect, useState } from "react";
import Data from "../data/Data";
import DataBulan from "../data/DataBulan";
import DataKhusus from "../data/DataKhusus";
import DataType from "../data/DataType";

const YnaContext = createContext();

export const useYna = () => useContext(YnaContext);

export const YnaProvider = ({ children }) => {
  const [totalSaldoSebelum2023, setTotalSaldoSebelum2023] = useState(56761000);
  const [totalSaldo, setTotalSaldo] = useState(0);
  const [dataFilter, setDataFilter] = useState(Data);
  const [selectKhusus, setSelectKhusus] = useState(DataKhusus);
  const [selectType, setSelectType] = useState(DataType);
  const [selectBulan, setSelectBulan] = useState(DataBulan);
  const [selectedKhusus, setSelectedKhusus] = useState("");
  const [selectedType, setSelectedType] = useState("");
  const [selectedBulan, setSelectedBulan] = useState("");
  const [sortType, setSortType] = useState("palingLama");

  const filterKhusus = (id) => {
    setSelectedKhusus(id);
  };

  const filterType = (id) => {
    setSelectedType(id);
  };

  const filterBulan = (bulanTahun) => {
    setSelectedBulan(bulanTahun);
  };

  function calculateTotalSaldo() {
    let tSaldo = 0;
    for (let i = 0; i < Data.length; i++) {
      const item = Data[i];
      tSaldo += item.pemasukan - item.pengeluaran;
    }
    setTotalSaldo(tSaldo);
  }

  useEffect(() => {
    let filteredData = Data;

    if (sortType === "palingBaru") {
      filteredData = filteredData.sort(
        (a, b) =>
          new Date(b.tanggal.split("-").reverse()) -
          new Date(a.tanggal.split("-").reverse())
      );
    } else if (sortType === "palingLama") {
      filteredData = filteredData.sort(
        (a, b) =>
          new Date(a.tanggal.split("-").reverse()) -
          new Date(b.tanggal.split("-").reverse())
      );
    }

    if (selectedKhusus !== "") {
      filteredData = filteredData.filter(
        (item) => item.khusus_id === parseInt(selectedKhusus)
      );
    }

    if (selectedType !== "") {
      filteredData = filteredData.filter(
        (item) => item.type_id === parseInt(selectedType)
      );
    }

    if (selectedBulan !== "") {
      const [bulan, tahun] = selectedBulan.split("-");
      filteredData = filteredData.filter((d) => {
        const [tgl, bln, thn] = d.tanggal.split("-");
        return `${bln}-${thn}` === `${bulan}-${tahun}`;
      });
    }

    setDataFilter(filteredData);
    calculateTotalSaldo();
  }, [sortType, selectedKhusus, selectedType, selectedBulan, totalSaldo]);

  return (
    <YnaContext.Provider
      value={{
        dataFilter,
        setDataFilter,
        selectKhusus,
        selectType,
        selectBulan,
        filterKhusus,
        filterType,
        filterBulan,
        setSortType,
        totalSaldoSebelum2023,
        totalSaldo,
      }}
    >
      {children}
    </YnaContext.Provider>
  );
};
