const DataBulan = [
  {
    id: "01-2023",
    nama: "Januari 2023",
  },
  {
    id: "02-2023",
    nama: "Februari 2023",
  },
  {
    id: "03-2023",
    nama: "Maret 2023",
  },
  {
    id: "04-2023",
    nama: "April 2023",
  },
];

export default DataBulan;
